<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Homepage</title>
</head>
<body>
<div style="display:flex;">
    <form action="/money-transfer/mt" method="post" style="width:50%;">
        <label for="xmlContent" style="display:block">Input pacs008 here:</label>
        <textarea id="xmlContent" name="xmlContent" rows="50" cols="90"></textarea>
        <button type="submit">generate pacs002</button>
    </form>
    <c:if test=${not empty parseResult}>
        <label for="output" style="display:block">Output pacs002:</label>
        <textarea id="output" name="xmlContent" rows="50" cols="90" readonly >${requestScope['parseResult']}</textarea>
    </c:if>
</div>
</body>
</html>