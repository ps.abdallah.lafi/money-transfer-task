package com.example.servlets;

import com.example.implementation.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/mt"})
public class MainPageServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String xmlContent = req.getParameter("xmlContent");
//        TransferRequestHandler transferRequestHandler = new TransferRequestHandler(
//                new TransferRequestParser(),
//                new TransferRequestValidator(),
//                new TransferRequestSerializer(),
//                new CompletedTransfersService(MainDatabaseService.getInstance()),
//                new RejectionReasonService()
//        );
//        String outputXML = transferRequestHandler.handle(xmlContent);
//        req.setAttribute("parseResult", outputXML);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/Index.jsp");
        requestDispatcher.forward(req, resp);
    }
}