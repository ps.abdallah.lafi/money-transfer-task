package com.example;

import com.example.controllers.MainController;
import com.example.implementation.TransferRequestHandler;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(MainController.class)
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransferRequestHandler transferRequestHandler;

    @Test
    public void givenXmlContent_whenGenerateResponse_thenReturnPageWithString() throws Exception {
        when(transferRequestHandler.handle(anyString())).thenReturn("Pacs002Message");
        XmlMessage xmlMessage = new XmlMessage();
        xmlMessage.setXmlContent("Pacs002Message");
        mockMvc.perform(post("/handler").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("xmlContent", xmlMessage.getXmlContent())).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Pacs002Message")));
    }

    @Test
    public void givenEmptyValue_whenGenerateResponse_thenReturnErrorPage() throws Exception {
        mockMvc.perform(post("/handler").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("xmlContent", "")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("The sent message is empty!")));
    }
}
