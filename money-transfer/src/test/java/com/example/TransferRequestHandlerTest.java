package com.example;

import com.example.implementation.*;
import com.example.entities.RejectionMotive;
import com.example.services.CompletedTransfersService;
import com.example.services.RejectionMotiveService;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.StringReader;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TransferRequestHandlerTest {

    @Test
    void givenValidPacs8_whenHandleParse_thenReturnAuthPac2() {
        TransferRequestValidator mockValidator = mock(TransferRequestValidator.class);
        when(mockValidator.validate(any(TransferRequestDetails.class))).thenReturn("valid");
        com.example.services.CompletedTransfersService mockCompletedTransfersService = mock(com.example.services.CompletedTransfersService.class);
        when(mockCompletedTransfersService.existsByMessageIdAndSettlementDateAndDebtorAgent(anyString(), anyString(), anyString())).thenReturn(false);
        RejectionMotiveService mockRejectionMotiveService = mock(RejectionMotiveService.class);
        when(mockRejectionMotiveService.getById(anyString())).thenReturn(new RejectionMotive("", "reason"));
        TransferRequestHandler transferRequestHandler = new TransferRequestHandler(
                new TransferRequestParser(),
                mockValidator,
                new TransferRequestSerializer(),
                mockCompletedTransfersService,
                mockRejectionMotiveService);
        assertTrue(compareLines(transferRequestHandler.handle(getValidPacs8()), getValidPacs2()));
    }

    @Test
    void givenInvalidPacs8_whenHandleParse_thenReturnNautPac2() {
        TransferRequestValidator mockValidator = mock(TransferRequestValidator.class);
        when(mockValidator.validate(any(TransferRequestDetails.class))).thenReturn("FF03");
        CompletedTransfersService mockCompletedTransfersService = mock(CompletedTransfersService.class);
        when(mockCompletedTransfersService.existsByMessageIdAndSettlementDateAndDebtorAgent(anyString(), anyString(), anyString())).thenReturn(false);
        RejectionMotiveService mockRejectionMotiveService = mock(RejectionMotiveService.class);
        when(mockRejectionMotiveService.getById(anyString())).thenReturn(new RejectionMotive("", "Payment Type Information is missing or invalid."));
        TransferRequestHandler transferRequestHandler = new TransferRequestHandler(
                new TransferRequestParser(),
                mockValidator,
                new TransferRequestSerializer(),
                mockCompletedTransfersService,
                mockRejectionMotiveService);
        assertTrue(compareLines(transferRequestHandler.handle(getInvalidPacs8()), getInvalidPacs2()));
    }

    private boolean compareLines(String firstString, String secondString) {
        try (BufferedReader firstReader = new BufferedReader(new StringReader(firstString));
             BufferedReader secondReader = new BufferedReader(new StringReader(secondString))) {
            String firstLine;
            String secondLine;
            while ((firstLine = firstReader.readLine()) != null)
                if ((secondLine = secondReader.readLine()) != null) {
                    if (firstLine.contains("CreDtTm") || firstLine.contains("MsgId"))
                        continue;
                    if (!firstLine.equals(secondLine))
                        return false;
                }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private String getValidPacs8() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.08\">\n" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA2812094750927334298</MsgId>\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2812094750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1.00</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>" + LocalDate.now() + "</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                <Nm>NISREEN JEHAD YOUSEF HUSSEIN</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                <Nm>MAEN HATTAR MH HATTAR</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }

    private String getValidPacs2() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.002.001.10\">\n" +
                "    <FIToFIPmtStsRpt>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>HBHOJOA0-1-211009112732-6443</MsgId>\n" +
                "            <CreDtTm>2022-05-08T09:15:23Z</CreDtTm>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <ClrSysMmbId>\n" +
                "                        <MmbId>ZYAAJOA0AIPS</MmbId>\n" +
                "                    </ClrSysMmbId>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "        </GrpHdr>\n" +
                "        <OrgnlGrpInfAndSts>\n" +
                "            <OrgnlMsgId>JGBA2812094750927334298</OrgnlMsgId>\n" +
                "            <OrgnlMsgNmId>pacs.008.001.08</OrgnlMsgNmId>\n" +
                "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>AUTH</Prtry>\n" +
                "                </Rsn>\n" +
                "            </StsRsnInf>\n" +
                "        </OrgnlGrpInfAndSts>\n" +
                "        <TxInfAndSts>\n" +
                "            <OrgnlInstrId>JGBA2812094750927334298</OrgnlInstrId>\n" +
                "            <OrgnlEndToEndId>NOTPROVIDED</OrgnlEndToEndId>\n" +
                "            <OrgnlTxId>JGBA2812094750927334297</OrgnlTxId>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <OrgnlTxRef>\n" +
                "                <IntrBkSttlmAmt Ccy=\"JOD\">1.00</IntrBkSttlmAmt>\n" +
                "                <IntrBkSttlmDt>" + LocalDate.now() + "</IntrBkSttlmDt>\n" +
                "            </OrgnlTxRef>\n" +
                "        </TxInfAndSts>\n" +
                "    </FIToFIPmtStsRpt>\n" +
                "</Document>\n";
    }

    private String getInvalidPacs8() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.08\">\n" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA2812094750927334298</MsgId>\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2812094750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1.00</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>2022-05-01</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                <Nm>NISREEN JEHAD YOUSEF HUSSEIN</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                <Nm>MAEN HATTAR MH HATTAR</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }

    private String getInvalidPacs2() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.002.001.10\">\n" +
                "    <FIToFIPmtStsRpt>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>HBHOJOA0-1-211009112732-6443</MsgId>\n" +
                "            <CreDtTm>2021-10-09T11:27:32Z</CreDtTm>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <ClrSysMmbId>\n" +
                "                        <MmbId>ZYAAJOA0AIPS</MmbId>\n" +
                "                    </ClrSysMmbId>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "        </GrpHdr>\n" +
                "        <OrgnlGrpInfAndSts>\n" +
                "            <OrgnlMsgId>JGBA2812094750927334298</OrgnlMsgId>\n" +
                "            <OrgnlMsgNmId>pacs.008.001.08</OrgnlMsgNmId>\n" +
                "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>NAUT</Prtry>\n" +
                "                </Rsn>\n" +
                "            </StsRsnInf>\n" +
                "        </OrgnlGrpInfAndSts>\n" +
                "        <TxInfAndSts>\n" +
                "            <OrgnlInstrId>JGBA2812094750927334298</OrgnlInstrId>\n" +
                "            <OrgnlEndToEndId>NOTPROVIDED</OrgnlEndToEndId>\n" +
                "            <OrgnlTxId>JGBA2812094750927334297</OrgnlTxId>\n" +
                "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>FF03</Prtry>\n" +
                "                </Rsn>\n" +
                "                <AddtlInf>Payment Type Information is missing or invalid.</AddtlInf>\n" +
                "            </StsRsnInf>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <OrgnlTxRef>\n" +
                "                <IntrBkSttlmAmt Ccy=\"JOD\">1.00</IntrBkSttlmAmt>\n" +
                "                <IntrBkSttlmDt>2022-05-01</IntrBkSttlmDt>\n" +
                "            </OrgnlTxRef>\n" +
                "        </TxInfAndSts>\n" +
                "    </FIToFIPmtStsRpt>\n" +
                "</Document>\n";
    }

}
