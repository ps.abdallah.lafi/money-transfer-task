package com.example;

import com.example.implementation.TransferRequestDetails;
import com.example.implementation.TransferRequestParser;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class TransferRequestParserTest {

    @Test
    void givenXMLFile_whenParseXMLFile_thenReturnXMLDetails() {
        TransferRequestParser transferParser = new TransferRequestParser();
        String xmlMessage = getValidPacs8();
        TransferRequestDetails xmlFileDetails = transferParser.parse(xmlMessage);
        assertMessageHeader(xmlFileDetails);
        assertMesseageId(xmlFileDetails);
        assertPaymentInfo(xmlFileDetails);
        assertAgents(xmlFileDetails);
        assertDebitorInfo(xmlFileDetails);
        assertCreditorInfo(xmlFileDetails);
    }

    private void assertMesseageId(TransferRequestDetails xmlFileDetails) {
        assertEquals("JGBA2812094750927334298", xmlFileDetails.getInstrId());
        assertEquals("NOTPROVIDED", xmlFileDetails.getEndToEndId());
        assertEquals("JGBA2812094750927334297", xmlFileDetails.getTxId());
    }

    private void assertMessageHeader(TransferRequestDetails xmlFileDetails) {
        assertEquals("JGBA2812094750927334298", xmlFileDetails.getMsgId());
        assertEquals("2020-12-28T07:47:50Z", xmlFileDetails.getCreDtTm().toString());
        assertEquals("1", xmlFileDetails.getNbOfTxs());
        assertEquals("CLRG", xmlFileDetails.getSttlmMtd());
    }

    private void assertAgents(TransferRequestDetails xmlFileDetails) {
        assertEquals("JGBAJOA0", xmlFileDetails.getInstgAgtBICFI());
        assertEquals("HBHOJOA0", xmlFileDetails.getInstdAgtBICFI());
        assertEquals("JGBAJOA0", xmlFileDetails.getDbtrAgtBICFI());
        assertEquals("HBHOJOA0", xmlFileDetails.getCdtrAgtBICFI());
    }

    private void assertCreditorInfo(TransferRequestDetails xmlFileDetails) {
        assertEquals("MAEN HATTAR MH HATTAR", xmlFileDetails.getCdtrNm());
        assertEquals("Amman Jordan SLT JO SLT", xmlFileDetails.getCdtrAdrLine());
        assertEquals("JO83HBHO0320000033330600101001", xmlFileDetails.getCdtrIBAN());
    }

    private void assertDebitorInfo(TransferRequestDetails xmlFileDetails) {
        assertEquals("NISREEN JEHAD YOUSEF HUSSEIN", xmlFileDetails.getDbtrNm());
        assertEquals("new zarqa", xmlFileDetails.getDbtrAdrLine());
        assertEquals("JO93JGBA6010000290450010010000", xmlFileDetails.getDbtrIBAN());
    }

    private void assertPaymentInfo(TransferRequestDetails xmlFileDetails) {
        assertEquals("RTNS", xmlFileDetails.getClrChanl());
        assertEquals("0100", xmlFileDetails.getSvcLvl());
        assertEquals("CSDC", xmlFileDetails.getLclInstrm());
        assertEquals("11110", xmlFileDetails.getCtgyPurp());
        assertEquals("JOD", xmlFileDetails.getCcy());
        assertEquals(0, xmlFileDetails.getIntrBkSttlmAmt().compareTo(BigDecimal.valueOf(1)));
        assertEquals(LocalDate.now().toString(), xmlFileDetails.getIntrBkSttlmDt().toString());
        assertEquals("SLEV", xmlFileDetails.getChrgBr());
    }

    private String getValidPacs8() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.08\">\n" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA2812094750927334298</MsgId>\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2812094750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1.00</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>" + LocalDate.now() + "</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                <Nm>NISREEN JEHAD YOUSEF HUSSEIN</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                <Nm>MAEN HATTAR MH HATTAR</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }
}