package com.example;

import com.example.implementation.IBANUtility;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class IBANUtilityTest {
    @Test
    void givenValidIBAN_whenValidateIBAN_returnTrue() {
        String validIBAN = "JO93JGBA6010000290450010010000";
        assertTrue(IBANUtility.validateIBAN(validIBAN));
    }

    @Test
    void givenInvalidIBAN_whenValidateIBAN_returnFalse() {
        String invalidIBAN = "JO93JGBA63242010000290450010010000";
        assertFalse(IBANUtility.validateIBAN(invalidIBAN));
    }
}
