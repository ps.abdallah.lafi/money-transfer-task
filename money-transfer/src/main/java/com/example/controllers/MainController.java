package com.example.controllers;

import com.example.XmlMessage;
import com.example.implementation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/handler")
public class MainController {

    private final TransferRequestHandler transferRequestHandler;

    @Autowired
    public MainController(TransferRequestHandler transferRequestHandler) {
        this.transferRequestHandler = transferRequestHandler;
    }

    @PostMapping
    public String handlePacs(XmlMessage xmlMessage, Model model) {
        if (xmlMessage.getXmlContent() == null || xmlMessage.getXmlContent().isEmpty())
            return "resultEmpty";
        model.addAttribute("xmlResult", transferRequestHandler.handle(xmlMessage.getXmlContent()));
        return "result";
    }
}
