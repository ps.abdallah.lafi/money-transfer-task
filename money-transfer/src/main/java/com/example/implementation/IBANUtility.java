package com.example.implementation;

public class IBANUtility {
    private IBANUtility() {
    }

    public static boolean validateIBAN(String iban) {
        final int IBAN_MIN_SIZE = 15;
        final int IBAN_MAX_SIZE = 34;
        final long IBAN_MAX = 999999999;
        final long IBAN_MODULUS = 97;
        String trimmed = iban.trim();
        if (trimmed.length() < IBAN_MIN_SIZE || trimmed.length() > IBAN_MAX_SIZE) {
            return false;
        }
        String reformat = trimmed.substring(4) + trimmed.substring(0, 4);
        long total = 0;
        for (int i = 0; i < reformat.length(); i++) {
            int charValue = Character.getNumericValue(reformat.charAt(i));
            if (charValue < 0 || charValue > 35) {
                return false;
            }
            total = (charValue > 9 ? total * 100 : total * 10) + charValue;
            if (total > IBAN_MAX) {
                total = (total % IBAN_MODULUS);
            }
        }
        return (total % IBAN_MODULUS) == 1;
    }
}