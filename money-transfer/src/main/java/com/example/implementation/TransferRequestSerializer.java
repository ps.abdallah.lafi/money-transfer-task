package com.example.implementation;

import com.example.pacs2.*;
import jakarta.xml.bind.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class TransferRequestSerializer {
    public String createXML(TransferRequestDetails transferRequestDetails) {
        Document output = fillXMLData(transferRequestDetails);
        if (transferRequestDetails.getRejectCode() == null)
            addAuthField(output);
        else
            addNautFields(transferRequestDetails.getRejectCode(), transferRequestDetails.getRejectDetails(), output);
        return marshalAndGetString(output);
    }

    private Document fillXMLData(TransferRequestDetails transferRequestDetails) {
        Document outputDoc = new Document();
        outputDoc.setFIToFIPmtStsRpt(new FIToFIPaymentStatusReportV10());
        outputDoc.getFIToFIPmtStsRpt().setGrpHdr(new GroupHeader91());
        GroupHeader91 groupHeader = outputDoc.getFIToFIPmtStsRpt().getGrpHdr();
        setDateTime(groupHeader);
        setGroupHeader(transferRequestDetails, groupHeader);
        setOriginalGroupInfo(transferRequestDetails, outputDoc);
        setTransactionInfo(transferRequestDetails, outputDoc);
        return outputDoc;
    }

    private void setTransactionInfo(TransferRequestDetails transferRequestDetails, Document outputDoc) {
        outputDoc.getFIToFIPmtStsRpt().getTxInfAndSts().add(new PaymentTransaction110());
        PaymentTransaction110 transactionInfo = outputDoc.getFIToFIPmtStsRpt().getTxInfAndSts().get(0);
        setAgents(transferRequestDetails, transactionInfo);
        transactionInfo.setOrgnlInstrId(transferRequestDetails.getInstrId());
        transactionInfo.setOrgnlEndToEndId(transferRequestDetails.getEndToEndId());
        transactionInfo.setOrgnlTxId(transferRequestDetails.getTxId());
        transactionInfo.setOrgnlTxRef(new OriginalTransactionReference28());
        transactionInfo.getOrgnlTxRef().setIntrBkSttlmAmt(new ActiveOrHistoricCurrencyAndAmount());
        transactionInfo.getOrgnlTxRef().getIntrBkSttlmAmt().setCcy(transferRequestDetails.getCcy());
        transactionInfo.getOrgnlTxRef().getIntrBkSttlmAmt().setValue(transferRequestDetails.getIntrBkSttlmAmt());
        transactionInfo.getOrgnlTxRef().setIntrBkSttlmDt(transferRequestDetails.getIntrBkSttlmDt());
    }

    private void setAgents(TransferRequestDetails transferRequestDetails, PaymentTransaction110 transactionInfo) {
        transactionInfo.setInstgAgt(new BranchAndFinancialInstitutionIdentification6());
        transactionInfo.getInstgAgt().setFinInstnId(new FinancialInstitutionIdentification18());
        transactionInfo.getInstgAgt().getFinInstnId().setBICFI(transferRequestDetails.getDbtrAgtBICFI());
        transactionInfo.setInstdAgt(new BranchAndFinancialInstitutionIdentification6());
        transactionInfo.getInstdAgt().setFinInstnId(new FinancialInstitutionIdentification18());
        transactionInfo.getInstdAgt().getFinInstnId().setBICFI(transferRequestDetails.getCdtrAgtBICFI());
    }

    private void setOriginalGroupInfo(TransferRequestDetails transferRequestDetails, Document outputDoc) {
        outputDoc.getFIToFIPmtStsRpt().getOrgnlGrpInfAndSts().add(new OriginalGroupHeader17());
        OriginalGroupHeader17 originalGroupInfo = outputDoc.getFIToFIPmtStsRpt().getOrgnlGrpInfAndSts().get(0);
        originalGroupInfo.setOrgnlMsgId(transferRequestDetails.getMsgId());
        originalGroupInfo.setOrgnlMsgNmId("pacs.008.001.08");
        originalGroupInfo.getStsRsnInf().add(new StatusReasonInformation12());
        originalGroupInfo.getStsRsnInf().get(0).setRsn(new StatusReason6Choice());
    }

    private void setGroupHeader(TransferRequestDetails transferRequestDetails, GroupHeader91 groupHeader) {
        groupHeader.setMsgId(generateMsgId(transferRequestDetails));
        groupHeader.setInstgAgt(new BranchAndFinancialInstitutionIdentification6());
        groupHeader.getInstgAgt().setFinInstnId(new FinancialInstitutionIdentification18());
        groupHeader.getInstgAgt().getFinInstnId().setBICFI(transferRequestDetails.getCdtrAgtBICFI());
        groupHeader.setInstdAgt(new BranchAndFinancialInstitutionIdentification6());
        groupHeader.getInstdAgt().setFinInstnId(new FinancialInstitutionIdentification18());
        groupHeader.getInstdAgt().getFinInstnId().setClrSysMmbId(new ClearingSystemMemberIdentification2());
        groupHeader.getInstdAgt().getFinInstnId().getClrSysMmbId().setMmbId("ZYAAJOA0AIPS");
    }

    private void setDateTime(GroupHeader91 groupHeader) {
        try {
            XMLGregorianCalendar dateTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());
            dateTime.setTime(dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond());
            groupHeader.setCreDtTm(dateTime.normalize());
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    private void addNautFields(String rejectCode, String rejectReason, Document output) {
        output.getFIToFIPmtStsRpt().getOrgnlGrpInfAndSts().get(0).getStsRsnInf().get(0).getRsn().setPrtry("NAUT");
        output.getFIToFIPmtStsRpt().getTxInfAndSts().get(0).getStsRsnInf().add(new StatusReasonInformation12());
        output.getFIToFIPmtStsRpt().getTxInfAndSts().get(0).getStsRsnInf().get(0).setRsn(new StatusReason6Choice());
        output.getFIToFIPmtStsRpt().getTxInfAndSts().get(0).getStsRsnInf().get(0).getRsn().setPrtry(rejectCode);
        output.getFIToFIPmtStsRpt().getTxInfAndSts().get(0).getStsRsnInf().get(0).getAddtlInf().add(rejectReason);
    }

    private void addAuthField(Document document) {
        document.getFIToFIPmtStsRpt().getOrgnlGrpInfAndSts().get(0).getStsRsnInf().get(0).getRsn().setPrtry("AUTH");
    }

    private String marshalAndGetString(Document outputDoc) {
        try {
            ObjectFactory objectFactory = new ObjectFactory();
            JAXBContext jaxbContext = JAXBContext.newInstance("com.example.pacs2");
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            JAXBElement<Document> xmlString = objectFactory.createDocument(outputDoc);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            marshaller.marshal(xmlString, outStream);
            return outStream.toString().trim();
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    private String generateMsgId(TransferRequestDetails moneyTransferData) {
        DateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        Date now = new Date();
        Random random = new Random();
        String bicfi = moneyTransferData.getInstdAgtBICFI();
        String dateTime = df.format(now);
        random.nextInt(9);
        int randomSingleDigit = random.nextInt(10);
        int randomQuadDigit = random.nextInt(9000) + 1000;
        return bicfi + "-" + randomSingleDigit + "-" + dateTime + "-" + randomQuadDigit;
    }
}