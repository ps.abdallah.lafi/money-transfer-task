package com.example.implementation;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

public class TransferRequestDetails {

    private String rejectCode;
    private String rejectDetails;
    private String msgId;
    private String nbOfTxs;
    private String instrId;
    private String endToEndId;
    private String txId;
    private String sttlmMtd;
    private XMLGregorianCalendar creDtTm;
    private String instgAgtBICFI;
    private String instdAgtBICFI;
    private String dbtrAgtBICFI;
    private String cdtrAgtBICFI;
    private String cdtrNm;
    private String cdtrAdrLine;
    private String cdtrIBAN;
    private String dbtrNm;
    private String dbtrAdrLine;
    private String dbtrIBAN;
    private String clrChanl;
    private String svcLvl;
    private String lclInstrm;
    private String ctgyPurp;
    private String ccy;
    private XMLGregorianCalendar intrBkSttlmDt;
    private String chrgBr;
    private BigDecimal intrBkSttlmAmt;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getNbOfTxs() {
        return nbOfTxs;
    }

    public void setNbOfTxs(String nbOfTxs) {
        this.nbOfTxs = nbOfTxs;
    }

    public String getInstrId() {
        return instrId;
    }

    public void setInstrId(String instrId) {
        this.instrId = instrId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getSttlmMtd() {
        return sttlmMtd;
    }

    public void setSttlmMtd(String sttlmMtd) {
        this.sttlmMtd = sttlmMtd;
    }

    public XMLGregorianCalendar getCreDtTm() {
        return creDtTm;
    }

    public void setCreDtTm(XMLGregorianCalendar creDtTm) {
        this.creDtTm = creDtTm;
    }

    public String getInstgAgtBICFI() {
        return instgAgtBICFI;
    }

    public void setInstgAgtBICFI(String instgAgtBICFI) {
        this.instgAgtBICFI = instgAgtBICFI;
    }

    public String getInstdAgtBICFI() {
        return instdAgtBICFI;
    }

    public void setInstdAgtBICFI(String instdAgtBICFI) {
        this.instdAgtBICFI = instdAgtBICFI;
    }

    public String getDbtrAgtBICFI() {
        return dbtrAgtBICFI;
    }

    public void setDbtrAgtBICFI(String dbtrAgtBICFI) {
        this.dbtrAgtBICFI = dbtrAgtBICFI;
    }

    public String getCdtrAgtBICFI() {
        return cdtrAgtBICFI;
    }

    public void setCdtrAgtBICFI(String cdtrAgtBICFI) {
        this.cdtrAgtBICFI = cdtrAgtBICFI;
    }

    public String getCdtrNm() {
        return cdtrNm;
    }

    public void setCdtrNm(String cdtrNm) {
        this.cdtrNm = cdtrNm;
    }

    public String getCdtrAdrLine() {
        return cdtrAdrLine;
    }

    public void setCdtrAdrLine(String cdtrAdrLine) {
        this.cdtrAdrLine = cdtrAdrLine;
    }

    public String getCdtrIBAN() {
        return cdtrIBAN;
    }

    public void setCdtrIBAN(String cdtrIBAN) {
        this.cdtrIBAN = cdtrIBAN;
    }

    public String getDbtrNm() {
        return dbtrNm;
    }

    public void setDbtrNm(String dbtrNm) {
        this.dbtrNm = dbtrNm;
    }

    public String getDbtrAdrLine() {
        return dbtrAdrLine;
    }

    public void setDbtrAdrLine(String dbtrAdrLine) {
        this.dbtrAdrLine = dbtrAdrLine;
    }

    public String getDbtrIBAN() {
        return dbtrIBAN;
    }

    public void setDbtrIBAN(String dbtrIBAN) {
        this.dbtrIBAN = dbtrIBAN;
    }

    public String getClrChanl() {
        return clrChanl;
    }

    public void setClrChanl(String clrChanl) {
        this.clrChanl = clrChanl;
    }

    public String getSvcLvl() {
        return svcLvl;
    }

    public void setSvcLvl(String svcLvl) {
        this.svcLvl = svcLvl;
    }

    public String getLclInstrm() {
        return lclInstrm;
    }

    public void setLclInstrm(String lclInstrm) {
        this.lclInstrm = lclInstrm;
    }

    public String getCtgyPurp() {
        return ctgyPurp;
    }

    public void setCtgyPurp(String ctgyPurp) {
        this.ctgyPurp = ctgyPurp;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public XMLGregorianCalendar getIntrBkSttlmDt() {
        return intrBkSttlmDt;
    }

    public void setIntrBkSttlmDt(XMLGregorianCalendar intrBkSttlmDt) {
        this.intrBkSttlmDt = intrBkSttlmDt;
    }

    public String getChrgBr() {
        return chrgBr;
    }

    public void setChrgBr(String chrgBr) {
        this.chrgBr = chrgBr;
    }

    public BigDecimal getIntrBkSttlmAmt() {
        return intrBkSttlmAmt;
    }

    public void setIntrBkSttlmAmt(BigDecimal intrBkSttlmAmt) {
        this.intrBkSttlmAmt = intrBkSttlmAmt;
    }

    public String getRejectCode() {
        return rejectCode;
    }

    public void setRejectCode(String rejectCode) {
        this.rejectCode = rejectCode;
    }

    public String getRejectDetails() {
        return rejectDetails;
    }

    public void setRejectDetails(String rejectDetails) {
        this.rejectDetails = rejectDetails;
    }
}
