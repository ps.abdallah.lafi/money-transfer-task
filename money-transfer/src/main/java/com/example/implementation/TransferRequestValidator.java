package com.example.implementation;

import com.example.services.CompletedTransfersService;
import com.example.services.CurrenciesService;
import com.example.services.ParticipantsBicService;
import com.example.services.PurposeCodesService;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.implementation.ValidationCodes.*;

public class TransferRequestValidator {
    private final CompletedTransfersService completedTransfersService;
    private final ParticipantsBicService participantsBicService;
    private final PurposeCodesService purposeCodesService;
    private final CurrenciesService currenciesService;

    public TransferRequestValidator(CompletedTransfersService completedTransfersService, ParticipantsBicService participantsBicService, PurposeCodesService purposeCodesService, CurrenciesService currenciesService) {
        this.completedTransfersService = completedTransfersService;
        this.participantsBicService = participantsBicService;
        this.purposeCodesService = purposeCodesService;
        this.currenciesService = currenciesService;
    }

    public String validate(TransferRequestDetails transferRequestDetails) {
        if (!validateMessageInfo(transferRequestDetails))
            return FF03.name();
        if (!validatePaymentDetails(transferRequestDetails))
            return FF03.name();
        if (!validateAgents(transferRequestDetails))
            return FF03.name();

        ValidationCodes creditorValidation = validateCreditorAccount(transferRequestDetails);
        if (!creditorValidation.equals(valid))
            return creditorValidation.name();

        ValidationCodes debtorValidation = validateDebtorAccount(transferRequestDetails);
        if (!debtorValidation.equals(valid))
            return debtorValidation.name();
        if (isDuplicateRequest(transferRequestDetails))
            return FF03.name();
        return valid.name();
    }

    private boolean isDuplicateRequest(TransferRequestDetails transferRequestDetails) {
        return completedTransfersService.existsByMessageIdAndSettlementDateAndDebtorAgent(
                transferRequestDetails.getMsgId(),
                transferRequestDetails.getIntrBkSttlmDt().toString(),
                transferRequestDetails.getDbtrAgtBICFI()
        );
    }

    private boolean validateAgents(TransferRequestDetails transferRequestDetails) {
        if (transferRequestDetails.getInstgAgtBICFI() == null)
            return false;
        if (!participantsBicService.existsById(transferRequestDetails.getInstgAgtBICFI()))
            return false;
        if (!transferRequestDetails.getCdtrAgtBICFI().equals(transferRequestDetails.getInstdAgtBICFI()))
            return false;
        if (transferRequestDetails.getInstdAgtBICFI() == null)
            return false;
        if (!participantsBicService.existsById(transferRequestDetails.getInstdAgtBICFI()))
            return false;
        return transferRequestDetails.getDbtrAgtBICFI().equals(transferRequestDetails.getInstgAgtBICFI());
    }

    private boolean validateMessageInfo(TransferRequestDetails transferRequestDetails) {
        if (transferRequestDetails.getMsgId() == null)
            return false;
        if (transferRequestDetails.getCreDtTm() == null)
            return false;
        if (transferRequestDetails.getNbOfTxs() == null)
            return false;
        if (transferRequestDetails.getSttlmMtd() == null)
            return false;
        if (transferRequestDetails.getInstrId() == null)
            return false;
        if (transferRequestDetails.getEndToEndId() == null)
            return false;
        if (transferRequestDetails.getTxId() == null)
            return false;
        if (transferRequestDetails.getClrChanl() == null)
            return false;
        if (transferRequestDetails.getSvcLvl() == null)
            return false;
        if (transferRequestDetails.getLclInstrm() == null)
            return false;
        if (transferRequestDetails.getCtgyPurp() == null)
            return false;
        if (!purposeCodesService.existsById(transferRequestDetails.getCtgyPurp()))
            return false;
        return transferRequestDetails.getChrgBr() != null;
    }

    private ValidationCodes validateCreditorAccount(TransferRequestDetails transferRequestDetails) {
        if (transferRequestDetails.getCdtrAgtBICFI() == null)
            return FF03;
        if (!participantsBicService.existsById(transferRequestDetails.getCdtrAgtBICFI()))
            return FF03;
        if (transferRequestDetails.getCdtrIBAN() == null)
            return AC03;
        if (!IBANUtility.validateIBAN(transferRequestDetails.getCdtrIBAN()))
            return AC03;
        if (transferRequestDetails.getCdtrNm() == null)
            return ACDM;
        if (transferRequestDetails.getCdtrNm().split(" ").length < 3)
            return ACDM;
        if (transferRequestDetails.getCdtrAdrLine() == null)
            return ACDM;
        if (transferRequestDetails.getCdtrAdrLine().split(" ").length < 2)
            return ACDM;
        return valid;
    }

    private ValidationCodes validateDebtorAccount(TransferRequestDetails transferRequestDetails) {
        if (transferRequestDetails.getDbtrIBAN() == null)
            return AC02;
        if (!IBANUtility.validateIBAN(transferRequestDetails.getDbtrIBAN()))
            return AC02;
        if (transferRequestDetails.getDbtrAgtBICFI() == null)
            return FF03;
        if (!participantsBicService.existsById(transferRequestDetails.getDbtrAgtBICFI()))
            return FF03;
        if (transferRequestDetails.getDbtrNm() == null)
            return ACDM;
        if (transferRequestDetails.getDbtrNm().split(" ").length < 3)
            return ACDM;
        if (transferRequestDetails.getDbtrAdrLine() == null)
            return ACDM;
        if (transferRequestDetails.getDbtrAdrLine().split(" ").length < 2)
            return ACDM;
        return valid;
    }

    private boolean validatePaymentDetails(TransferRequestDetails transferRequestDetails) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String expectedDate = simpleDateFormat.format(new Date());
        String settlementDate;
        if (transferRequestDetails.getIntrBkSttlmDt() != null)
            settlementDate = transferRequestDetails.getIntrBkSttlmDt().toString();
        else
            throw new IllegalArgumentException("Couldn't build pacs002");

        BigDecimal settlementAmount = transferRequestDetails.getIntrBkSttlmAmt();
        if (transferRequestDetails.getCcy() == null)
            return false;
        if (!currenciesService.existsById(transferRequestDetails.getCcy()))
            return false;
        if (transferRequestDetails.getIntrBkSttlmAmt() == null)
            return false;
        if (settlementAmount.compareTo(BigDecimal.ZERO) <= 0)
            return false;
        if (settlementAmount.scale() != 2)
            return false;
        return areDatesEqual(expectedDate, settlementDate);
    }

    private boolean areDatesEqual(String expectedDate, String settlementDate) {
        return expectedDate.equals(settlementDate);
    }
}