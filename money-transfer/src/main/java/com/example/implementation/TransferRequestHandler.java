package com.example.implementation;

import com.example.entities.CompletedTransfers;
import com.example.entities.RejectionMotive;
import com.example.services.CompletedTransfersService;
import com.example.services.RejectionMotiveService;

public class TransferRequestHandler {
    private final TransferRequestParser transferRequestParser;
    private final TransferRequestValidator transferRequestValidator;
    private final TransferRequestSerializer transferRequestSerializer;
    private final CompletedTransfersService completedTransfersService;
    private final RejectionMotiveService rejectionMotiveService;

    public TransferRequestHandler(TransferRequestParser transferRequestParser, TransferRequestValidator transferRequestValidator, TransferRequestSerializer transferRequestSerializer, CompletedTransfersService completedTransfersService, RejectionMotiveService rejectionMotiveService) {
        this.transferRequestParser = transferRequestParser;
        this.transferRequestValidator = transferRequestValidator;
        this.transferRequestSerializer = transferRequestSerializer;
        this.completedTransfersService = completedTransfersService;
        this.rejectionMotiveService = rejectionMotiveService;
    }

    public String handle(String xmlMessage) {
        TransferRequestDetails parsedMessage = parseMessage(xmlMessage);
        String validationResult = validateParsedMessage(parsedMessage);
        if (validationResult.equals("valid"))
            saveToDatabase(parsedMessage.getMsgId(),
                    parsedMessage.getIntrBkSttlmDt().toString(),
                    parsedMessage.getDbtrAgtBICFI());
        else {
            parsedMessage.setRejectCode(validationResult);
            parsedMessage.setRejectDetails(fetchRejectReason(validationResult));
        }
        return transferRequestSerializer.createXML(parsedMessage);
    }

    private void saveToDatabase(String msgId, String settlementDate, String debtorAgent) {
        CompletedTransfers transferDetails = new CompletedTransfers(msgId, settlementDate, debtorAgent);
        completedTransfersService.save(transferDetails);
    }

    private String validateParsedMessage(TransferRequestDetails parsedMessage) {
        return transferRequestValidator.validate(parsedMessage);
    }

    private TransferRequestDetails parseMessage(String xmlMessage) {
        return transferRequestParser.parse(xmlMessage);
    }

    private String fetchRejectReason(String rejectCode) {
        RejectionMotive rejectionMotive = rejectionMotiveService.getById(rejectCode);
        return rejectionMotive.getDetails();
    }
}