package com.example.implementation;

import com.example.pacs8.*;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import java.io.StringReader;
import java.util.List;

public class TransferRequestParser {
    public TransferRequestDetails parse(String xmlMessage) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance("com.example.pacs8");
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            JAXBElement<Document> jaxbElement = (JAXBElement<Document>) unmarshaller.unmarshal(new StringReader(xmlMessage));
            return fillDetails(jaxbElement.getValue());

        } catch (JAXBException e) {
            throw new RuntimeException("Invalid input format", e);
        }
    }

    private TransferRequestDetails fillDetails(Document parsedDetails) {
        TransferRequestDetails transferRequestDetails = new TransferRequestDetails();
        GroupHeader93 groupHeader = parsedDetails.getFIToFICstmrCdtTrf().getGrpHdr();
        CreditTransferTransaction39 transactionInfo = parsedDetails.getFIToFICstmrCdtTrf().getCdtTrfTxInf().get(0);
        setGroupHeader(transferRequestDetails, groupHeader);
        setPmtId(transferRequestDetails, transactionInfo.getPmtId());
        setPmtTpInf(transferRequestDetails, transactionInfo.getPmtTpInf());
        setIntrBkSttlm(transferRequestDetails, transactionInfo);
        setAgents(transferRequestDetails, transactionInfo);
        setDebtorDetails(transferRequestDetails, transactionInfo);
        setCreditorDetails(transferRequestDetails, transactionInfo);
        return transferRequestDetails;
    }

    private void setCreditorDetails(TransferRequestDetails transferRequestDetails, CreditTransferTransaction39 transactionInfo) {
        transferRequestDetails.setCdtrNm(transactionInfo.getCdtr().getNm());
        transferRequestDetails.setCdtrIBAN(transactionInfo.getCdtrAcct().getId().getIBAN());
        List<String> cdtrAdrLines = transactionInfo.getCdtr().getPstlAdr().getAdrLine();
        String cdtrAdrLine = concatAdrLines(cdtrAdrLines);
        transferRequestDetails.setCdtrAdrLine(cdtrAdrLine);
    }

    private void setDebtorDetails(TransferRequestDetails transferRequestDetails, CreditTransferTransaction39 transactionInfo) {
        transferRequestDetails.setDbtrNm(transactionInfo.getDbtr().getNm());
        transferRequestDetails.setDbtrIBAN(transactionInfo.getDbtrAcct().getId().getIBAN());
        List<String> dbtrAdrLines = transactionInfo.getDbtr().getPstlAdr().getAdrLine();
        String dbtrAdrLine = concatAdrLines(dbtrAdrLines);
        transferRequestDetails.setDbtrAdrLine(dbtrAdrLine);
    }

    private String concatAdrLines(List<String> adrLines) {
        String resultAdrLine = "";
        for (String adrLine : adrLines) {
            resultAdrLine = resultAdrLine.concat(adrLine);
        }
        return resultAdrLine;
    }

    private void setAgents(TransferRequestDetails transferRequestDetails, CreditTransferTransaction39 transactionInfo) {
        transferRequestDetails.setInstgAgtBICFI(transactionInfo.getInstgAgt().getFinInstnId().getBICFI());
        transferRequestDetails.setInstdAgtBICFI(transactionInfo.getInstdAgt().getFinInstnId().getBICFI());
        transferRequestDetails.setDbtrAgtBICFI(transactionInfo.getDbtrAgt().getFinInstnId().getBICFI());
        transferRequestDetails.setCdtrAgtBICFI(transactionInfo.getCdtrAgt().getFinInstnId().getBICFI());
    }

    private void setIntrBkSttlm(TransferRequestDetails transferRequestDetails, CreditTransferTransaction39 transactionInfo) {
        transferRequestDetails.setCcy(transactionInfo.getIntrBkSttlmAmt().getCcy());
        transferRequestDetails.setIntrBkSttlmAmt(transactionInfo.getIntrBkSttlmAmt().getValue());
        transferRequestDetails.setIntrBkSttlmDt(transactionInfo.getIntrBkSttlmDt());
        transferRequestDetails.setChrgBr(transactionInfo.getChrgBr().value());
    }

    private void setPmtTpInf(TransferRequestDetails transferRequestDetails, PaymentTypeInformation28 paymentTypeInfo) {
        transferRequestDetails.setClrChanl(paymentTypeInfo.getClrChanl().value());
        transferRequestDetails.setSvcLvl(paymentTypeInfo.getSvcLvl().get(0).getPrtry());
        transferRequestDetails.setLclInstrm(paymentTypeInfo.getLclInstrm().getPrtry());
        transferRequestDetails.setCtgyPurp(paymentTypeInfo.getCtgyPurp().getPrtry());
    }

    private void setPmtId(TransferRequestDetails transferRequestDetails, PaymentIdentification7 paymentId) {
        transferRequestDetails.setInstrId(paymentId.getInstrId());
        transferRequestDetails.setEndToEndId(paymentId.getEndToEndId());
        transferRequestDetails.setTxId(paymentId.getTxId());
    }

    private void setGroupHeader(TransferRequestDetails transferRequestDetails, GroupHeader93 groupHeader) {
        transferRequestDetails.setMsgId(groupHeader.getMsgId());
        transferRequestDetails.setCreDtTm(groupHeader.getCreDtTm());
        transferRequestDetails.setNbOfTxs(groupHeader.getNbOfTxs());
        transferRequestDetails.setSttlmMtd(groupHeader.getSttlmInf().getSttlmMtd().value());
    }
}