package com.example.services;

import com.example.repositories.ParticipantsBicRepository;

public class ParticipantsBicService {
    private final ParticipantsBicRepository participantsBicRepository;

    public ParticipantsBicService(ParticipantsBicRepository participantsBicRepository) {
        this.participantsBicRepository = participantsBicRepository;
    }

    public boolean existsById(String BICFI) {
        return participantsBicRepository.existsById(BICFI);
    }
}
