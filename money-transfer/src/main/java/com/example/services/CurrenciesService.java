package com.example.services;

import com.example.repositories.CurrenciesRepository;

public class CurrenciesService {
    private final CurrenciesRepository currenciesRepository;

    public CurrenciesService(CurrenciesRepository currenciesRepository) {
        this.currenciesRepository = currenciesRepository;
    }

    public boolean existsById(String ccy) {
        return currenciesRepository.existsById(ccy);
    }
}
