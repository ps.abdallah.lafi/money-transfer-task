package com.example.services;

import com.example.entities.RejectionMotive;
import com.example.repositories.RejectionMotiveRepository;

public class RejectionMotiveService {

    private final RejectionMotiveRepository rejectionMotiveRepository;

    public RejectionMotiveService(RejectionMotiveRepository rejectionMotiveRepository) {
        this.rejectionMotiveRepository = rejectionMotiveRepository;
    }

    public RejectionMotive getById(String rejectCode) {
        return rejectionMotiveRepository.getById(rejectCode);
    }
}
