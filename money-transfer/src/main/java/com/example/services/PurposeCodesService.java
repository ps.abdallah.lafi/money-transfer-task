package com.example.services;

import com.example.repositories.PurposeCodesRepository;

public class PurposeCodesService {
    private final PurposeCodesRepository purposeCodesRepository;

    public PurposeCodesService(PurposeCodesRepository purposeCodesRepository) {
        this.purposeCodesRepository = purposeCodesRepository;
    }

    public boolean existsById(String ctgyPurp) {
        return purposeCodesRepository.existsById(ctgyPurp);
    }
}
