package com.example.services;

import com.example.entities.CompletedTransfers;
import com.example.repositories.CompletedTransfersRepository;

public class CompletedTransfersService {
    private final CompletedTransfersRepository completedTransfersRepository;

    public CompletedTransfersService(CompletedTransfersRepository completedTransfersRepository) {
        this.completedTransfersRepository = completedTransfersRepository;
    }

    public void save(CompletedTransfers transferDetails) {
        completedTransfersRepository.save(transferDetails);
    }

    public boolean existsByMessageIdAndSettlementDateAndDebtorAgent(String messageId, String settlementDate, String debtorAgent) {
        return completedTransfersRepository.existsByMessageIdAndSettlementDateAndDebtorAgent(messageId,settlementDate,debtorAgent);
    }
}
