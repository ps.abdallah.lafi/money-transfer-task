package com.example.repositories;

import com.example.entities.PurposeCodes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurposeCodesRepository extends JpaRepository<PurposeCodes, String> {

}
