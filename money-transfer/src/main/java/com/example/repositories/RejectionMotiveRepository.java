package com.example.repositories;

import com.example.entities.RejectionMotive;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RejectionMotiveRepository extends JpaRepository<RejectionMotive, String> {

}
