package com.example.repositories;

import com.example.entities.CompletedTransfers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompletedTransfersRepository extends JpaRepository<CompletedTransfers, String> {
    boolean existsByMessageIdAndSettlementDateAndDebtorAgent(String messageId, String settlementDate, String debtorAgent);
}
