package com.example.repositories;

import com.example.entities.ParticipantsBic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticipantsBicRepository extends JpaRepository<ParticipantsBic, String> {

    }
