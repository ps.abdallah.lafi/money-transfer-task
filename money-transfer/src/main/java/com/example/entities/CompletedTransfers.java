package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "CompletedTransfers")
@Table(name = "completed_transfers")
@IdClass(CompletedTransfers.class)
public class CompletedTransfers implements Serializable {
    @Id
    @Column(name = "message_id")
    String messageId;
    @Id
    @Column(name = "settlement_date")
    String settlementDate;
    @Id
    @Column(name = "debtor_agent")
    String debtorAgent;

    public CompletedTransfers() {
    }

    public CompletedTransfers(String messageId, String settlementDate, String debtorAgent) {
        this.messageId = messageId;
        this.settlementDate = settlementDate;
        this.debtorAgent = debtorAgent;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getDebtorAgent() {
        return debtorAgent;
    }

    public void setDebtorAgent(String debtorAgent) {
        this.debtorAgent = debtorAgent;
    }
}
