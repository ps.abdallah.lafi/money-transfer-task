package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "RejectionMotive")
@Table(name = "rejection_motive")
public class RejectionMotive {

    @Id
    @Column(name = "rejection_code")
    String code;
    @Column(name = "rejection_details")
    String details;

    public RejectionMotive() {
    }

    public RejectionMotive(String code, String details) {
        this.code = code;
        this.details = details;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
