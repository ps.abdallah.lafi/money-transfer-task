package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "ParticipantsBic")
@Table(name = "participants_bic")
public class ParticipantsBic {

    @Id
    @Column(name = "code")
    String code;
    @Column(name = "participant")
    String participant;

    public ParticipantsBic() {
    }

    public ParticipantsBic(String code, String participant) {
        this.code = code;
        this.participant = participant;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }
}
