package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "PurposeCodes")
@Table(name = "purpose_codes")
public class PurposeCodes {

    @Id
    @Column(name = "code")
    String code;
    @Column(name = "purpose")
    String purpose;

    public PurposeCodes() {
    }

    public PurposeCodes(String code, String purpose) {
        this.code = code;
        this.purpose = purpose;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
}
