package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Currencies")
@Table(name = "Currencies")
public class Currencies {

    @Id
    @Column(name = "name")
    String Name;
    @Column(name = "format")
    String Format;

    public Currencies() {
    }

    public Currencies(String name, String format) {
        Name = name;
        Format = format;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFormat() {
        return Format;
    }

    public void setFormat(String format) {
        Format = format;
    }
}


