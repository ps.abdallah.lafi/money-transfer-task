package com.example.configurations;

import com.example.repositories.RejectionMotiveRepository;
import com.example.services.RejectionMotiveService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RejectionMotiveServiceConfiguration {

    @Bean
    public RejectionMotiveService rejectionMotiveService(RejectionMotiveRepository rejectionMotiveRepository) {
        return new RejectionMotiveService(rejectionMotiveRepository);
    }
}
