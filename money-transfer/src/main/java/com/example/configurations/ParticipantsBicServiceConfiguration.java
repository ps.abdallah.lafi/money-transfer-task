package com.example.configurations;

import com.example.repositories.ParticipantsBicRepository;
import com.example.services.ParticipantsBicService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ParticipantsBicServiceConfiguration {

    @Bean
    public ParticipantsBicService participantsBicService(ParticipantsBicRepository participantsBicRepository) {
        return new ParticipantsBicService(participantsBicRepository);
    }
}
