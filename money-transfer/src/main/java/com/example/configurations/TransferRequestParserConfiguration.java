package com.example.configurations;

import com.example.implementation.TransferRequestParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TransferRequestParserConfiguration {

    @Bean
    public TransferRequestParser transferRequestParser() {
        return new TransferRequestParser();
    }
}
