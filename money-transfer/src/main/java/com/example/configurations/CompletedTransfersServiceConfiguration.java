package com.example.configurations;

import com.example.repositories.CompletedTransfersRepository;
import com.example.services.CompletedTransfersService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CompletedTransfersServiceConfiguration {

    @Bean
    public CompletedTransfersService completedTransfersService(CompletedTransfersRepository completedTransfersRepository) {
        return new CompletedTransfersService(completedTransfersRepository);
    }
}
