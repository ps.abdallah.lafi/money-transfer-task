package com.example.configurations;

import com.example.repositories.PurposeCodesRepository;
import com.example.services.PurposeCodesService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PurposeCodesServiceConfiguration {

    @Bean
    public PurposeCodesService purposeCodesService(PurposeCodesRepository purposeCodesRepository) {
        return new PurposeCodesService(purposeCodesRepository);
    }
}
