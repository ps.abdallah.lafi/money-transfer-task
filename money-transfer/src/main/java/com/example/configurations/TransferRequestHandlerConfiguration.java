package com.example.configurations;

import com.example.implementation.*;
import com.example.services.CompletedTransfersService;
import com.example.services.RejectionMotiveService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TransferRequestHandlerConfiguration {

    @Bean
    public TransferRequestHandler transferRequestHandler(TransferRequestParser transferRequestParser,
                                                         TransferRequestValidator transferRequestValidator,
                                                         TransferRequestSerializer transferRequestSerializer,
                                                         CompletedTransfersService completedTransfersService,
                                                         RejectionMotiveService rejectionMotiveService) {
        return new TransferRequestHandler(transferRequestParser,
                transferRequestValidator,
                transferRequestSerializer,
                completedTransfersService,
                rejectionMotiveService);
    }
}
