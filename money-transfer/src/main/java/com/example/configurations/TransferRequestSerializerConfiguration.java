package com.example.configurations;

import com.example.implementation.TransferRequestSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TransferRequestSerializerConfiguration {

    @Bean
    public TransferRequestSerializer transferRequestSerializer() {
        return new TransferRequestSerializer();
    }
}
