package com.example.configurations;

import com.example.repositories.CurrenciesRepository;
import com.example.services.CurrenciesService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CurrenciesServiceConfiguration {

    @Bean
    public CurrenciesService currenciesService(CurrenciesRepository currenciesRepository) {
        return new CurrenciesService(currenciesRepository);
    }
}
