package com.example.configurations;

import com.example.implementation.TransferRequestValidator;
import com.example.services.CompletedTransfersService;
import com.example.services.CurrenciesService;
import com.example.services.ParticipantsBicService;
import com.example.services.PurposeCodesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TransferRequestValidatorConfiguration {

    @Bean
    @Autowired
    public TransferRequestValidator transferRequestValidator(CompletedTransfersService completedTransfersService,
                                                             ParticipantsBicService participantsBicService,
                                                             PurposeCodesService purposeCodesService,
                                                             CurrenciesService currenciesService) {
        return new TransferRequestValidator(completedTransfersService, participantsBicService, purposeCodesService, currenciesService);
    }
}
